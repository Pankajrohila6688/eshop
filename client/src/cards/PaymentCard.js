import React from 'react';
import './PaymentCard.css';

function PaymentCard({ image }) {
    return (
        <div className="paymentCard">
            <img src={image} alt="" />
        </div>
    )
}


export default PaymentCard;
