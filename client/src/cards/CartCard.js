import React from 'react';
import './CartCard.css';

function CartCard({ name, description, number, price, image }) {
    return (
        <div className="cartCard">
            <img src={image} alt="" />

            <div className="cartCard__text">
                <p>{name}</p>
                <p>{description}</p>
                <p>{number}</p>
            </div>

            <p>$ {price}</p>
        </div>
    )
}

export default CartCard;
