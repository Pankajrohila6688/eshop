import React from 'react';
import './DeliveryCard.css';

function DeliveryCard({ image, price }) {
    return (
        <div className="deliveryCard">
            <img src={image} alt="" />
            <p>$ {price}</p>
        </div>
    )
}

export default DeliveryCard;
