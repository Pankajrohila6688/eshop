import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './styles/Register.css'

function Login() {
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')

	const navigate = useNavigate();

	async function loginUser(event) {
		event.preventDefault()

		const response = await fetch('http://localhost:3001/api/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				email,
				password,
			}),
		})
		const data = await response.json()

		console.log(data);

		if (data.user) {
			localStorage.setItem('token', data.user)
			console.log(data.user)
			alert('Login successful')
			navigate('/')
		} else {
			alert('Please check your username and password')
			navigate('/')
		}
	}

	return (
		<div className="register">
			<h1>Login</h1>
			<form onSubmit={loginUser}>
				<input
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					type="email"
					placeholder="Email"
				/>
				<br />
				<input
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					type="password"
					placeholder="Password"
				/>
				<br />
				<input type="submit" value="Login" />
			</form>
		</div>
	)
}

export default Login;