import React, {useState} from 'react'
import { useNavigate } from 'react-router-dom';
import "./styles/Register.css";

function Register() {

    const [name, setName] = useState('')
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('');

    const navigate = useNavigate();

    async function registerUser(event) {
		event.preventDefault()

		const response = await fetch('http://localhost:3001/api/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				name,
				email,
				password,
			}),
		})

		const data = await response.json()

		console.log(data)

		if (data.status === 'ok') {
			navigate('/')
			alert('User registered successfully!!')
		}
	}

    return (
        <div className="register">
            <h1>Register</h1>
            <form onSubmit={registerUser}>
				<input
					value={name}
					required
					onChange={(e) => setName(e.target.value)}
					type="text"
					placeholder="Name"
				/>
				<br />
				<input
					value={email}
					required
					onChange={(e) => setEmail(e.target.value)}
					type="email"
					placeholder="Email"
				/>
				<br />
				<input
					value={password}
					required
					onChange={(e) => setPassword(e.target.value)}
					type="password"
					placeholder="Password"
				/>
				<br />
				<input style={{ cursor: 'pointer' }} type="submit" value="Register" />
			</form>
        </div>
    )
}

export default Register;
