import React from 'react';
import PaymentCard from '../cards/PaymentCard';
import DeliveryCard from '../cards/DeliveryCard';
import './styles/Payment.css';

function Payment() {
    return (
        <div className="payment">
            <p>Payment method</p>

            <div className="payment__paymentCard">
                <PaymentCard image="/images/paypal.png" />
                <PaymentCard image="/images/visa.png" />
                <PaymentCard image="/images/mastercard.png" />
                <PaymentCard image="/images/maestro.png" />
                <PaymentCard image="/images/discover.png" />
                <PaymentCard image="/images/ideal.png" />
            </div>

            <p>Delivery method</p>

            <div className="payment__deliveryCard">
                <DeliveryCard image="/images/inpost.png" price="20.00" />
                <DeliveryCard image="/images/dpd.png" price="12.00" />
                <DeliveryCard image="/images/dhl.png" price="15.00" />
                <DeliveryCard image="/images/fedex.png" price="10.00" />
            </div>
        </div>
    )
}

export default Payment;
