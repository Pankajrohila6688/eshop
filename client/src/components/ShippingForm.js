import React from 'react';
import './styles/ShippingForm.css';
import ArrowBackOutlinedIcon from '@mui/icons-material/ArrowBackOutlined';
import { useNavigate } from 'react-router-dom';

function ShippingForm() {

    const navigate = useNavigate();

    return (
        <div className="shippingForm">
            <h2>Shipping and Payment</h2>

            <div className="shippingForm__buttons">
                <button onClick={() => navigate('login')}>LOG IN</button>
                <button onClick={() => navigate('register')}>SIGN UP</button>
            </div>

            <p>Shipping information</p>

            <form className="shippingFormForm">
                <input placeholder="Email" />
                <input placeholder="Address" />
                <input placeholder="First name" />
                <input placeholder="City" />
                <input placeholder="Last name" />
                <input placeholder="Postal Code / ZIP" />
                <input placeholder="Phone number" />
                <input placeholder="Poland" />
            </form>

            <div className="shippingForm__backBtn">
                <ArrowBackOutlinedIcon />
                <p>Back</p>
            </div>
        </div>
    )
}

export default ShippingForm;
