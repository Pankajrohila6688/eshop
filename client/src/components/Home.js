import React, { useState } from 'react';
import './styles/Home.css';
import ShippingForm from './ShippingForm';
import LocalShippingOutlinedIcon from '@mui/icons-material/LocalShippingOutlined';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';
import Payment from './Payment';
import Cart from './Cart';

function loadScript(src) {
	return new Promise((resolve) => {
		const script = document.createElement('script')
		script.src = src
		script.onload = () => {
			resolve(true)
		}
		script.onerror = () => {
			resolve(false)
		}
		document.body.appendChild(script)
	})
}

const __DEV__ = document.domain === 'localhost'

function Home() {

    const [name, setName] = useState('username')

    async function displayRazorpay() {
		const res = await loadScript('https://checkout.razorpay.com/v1/checkout.js')

		if (!res) {
			alert('Razorpay SDK failed to load. Are you online?')
			return
		}

		const data = await fetch('http://localhost:3001/razorpay', { method: 'POST' }).then((t) =>
			t.json()
		)

		console.log(data)

		const options = {
			key: __DEV__ ? 'rzp_test_xTpUypDcTCobNs' : 'PRODUCTION_KEY',
			currency: data.currency,
			amount: data.amount.toString(),
			order_id: data.id,
			name: 'Shopping',
			description: 'Thank you for shopping with E-SHOP',
			handler: function (response) {
				alert(response.razorpay_payment_id)
				alert(response.razorpay_order_id)
				alert(response.razorpay_signature)
			},
			prefill: {
				name,
				email: 'userEmail@gmail.com',
				phone_number: '9899999999'
			}
		}
		const paymentObject = new window.Razorpay(options)
		paymentObject.open()
	}

    return (
        <div className="home">
            <div className="home__left">
                <ShippingForm />
            </div>

            <div className="home__right">
                <div className="home__rightIcons">
                    <ShoppingCartOutlinedIcon className="home__rightIcon" />
                    <div style={{ height: '20px', width: '80px', borderBottom: '1px solid lightgray', margin: '-2px 10px 0px 10px'  }} />
                    <LocalShippingOutlinedIcon className="home__rightIcon active" />
                </div>

                <div className="home__rightPayment">
                    <Payment />
                    <Cart />
                </div>

                <div className="home__rightBtns">
                    <button>CONTINUE SHOPPING</button>
                    <button onClick={displayRazorpay} >PROCEED TO PAYMENT</button>
                </div>
            </div>
        </div>
    )
}

export default Home;
