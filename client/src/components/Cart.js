import React from 'react';
import CartCard from '../cards/CartCard';
import './styles/Cart.css';
import LocalShippingOutlinedIcon from '@mui/icons-material/LocalShippingOutlined';

function Cart() {
    return (
        <div className="cart">
            <p style={{ marginBottom: '20px', fontSize: '15px', color: 'rgb(73, 68, 68)' }}>Your cart</p>

            <div>
                <CartCard
                    name="T-Shirt"
                    description="Summer Vibes" 
                    number="#261311"
                    price="89.99"
                    image="https://images.unsplash.com/photo-1586083702768-190ae093d34d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1000&q=80"
                />
                <CartCard 
                    name="Basic Slim"
                    description="Summer Vibes" 
                    number="#212315"
                    price="69.99"
                    image="https://images.unsplash.com/photo-1622519407650-3df9883f76a5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1000&q=80"
                />
            </div>

            <button className="cartBtn">
                <p>Total Cost</p>
                <p>$159.98</p>
            </button>

            <div className="cart__bottom">
                <LocalShippingOutlinedIcon />
                <p>You are $30.02 away <br /> from the shipping!</p>
            </div>
        </div>
    )
}

export default Cart;
