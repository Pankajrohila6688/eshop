import React from 'react';
import './styles//Header.css';
import SearchIcon from '@mui/icons-material/Search';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';
import PersonOutlineOutlinedIcon from '@mui/icons-material/PersonOutlineOutlined';
import { useNavigate } from 'react-router-dom';

function Header() {

    const navigate = useNavigate();

    return (
        <div className="header">
            <div className="header__left">
                <h3 style={{ cursor: 'pointer' }} onClick={() => navigate('/')}>E-SHOP</h3>
            </div>

            <div className="header__middle">
                <p>Men</p>
                <p>Women</p>
                <p>Kids</p>
            </div>

            <div className="header__right">
                <SearchIcon className="header__rightIcon"/>
                <ShoppingCartOutlinedIcon className="header__rightIcon" />
                <PersonOutlineOutlinedIcon className="header__rightIcon" />
            </div>
        </div>
    )
}

export default Header;
